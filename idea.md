# die idee

in der regel ist buchhaltung und steuer ein notwendiges übel. die meisten selbständigen sehen nehmen sie als gegeben und unveränderbar hin.
dabei ist die steuer genau genommen der größte "ausgabeposten" jeden betriebes und jeder privatperson.

nur wenige schöpfen aktiv die möglichkeiten aus, um steuern zu sparen und diese ausgabe aktiv in mehr cashflow zu wandeln. und damit geht ihnen ein u.u. wettbewerbsvorteil verloren.
denn je mit mehr freiem cashflow, kann das unternehmen schneller wachsen und das erwirtschaftete geld optimal genutzt werden.

dabei sind keine illegalen steuer-ticks gemeint, sondern legale möglichkeiten, die dem staat u.a. als werkzeuge zur lenkung der wirtschaft zur verfügung steht.
diese sind aber meist nicht im nachhinein einsetzbar, wenn man an der erklärung für das letzte jahr sitzt, sondern müssen im voraus geplant sein.
manchmal sind es auch einfach nur kleine anpassungen an ohnehin geplanten investitionen oder projekten, die einem schnell zahlungen von tausenden euros ersparen können.

das volle potential lässt sich nur ausschöpfen, wenn man als inhaber über ein sehr breites und tiefes steuerliches wissen verfügt, oder regelmäßig seinen steuerberater über kurz-, mittel- und langfristige ziele informiert.

da die meisten dieser dinge auf klar festgelegten bedingungen basieren, lassen sie sich auf der grundlage ausreichender informationen über geschehenem geschäft und zukünfigten planungen handlungsempfehungen festsetzen.

die idee ist es eine buchhaltungssoftware zu schaffen, die handlungsempfehlungen durch ein KI gestütztes programm empfängt.
die handlungsempfehlungen können zum einen automatisch laufen und zum anderen als basis für planungen und gespräche mit dem steuerberater dienen.

Die herausforderung liegt in der auswertugn der daten. Es muss aus den buchhalterischen zahlen in verbindung mit den Eingaben zu den Plänen des Inhabers perspektivisch steuerliche vorteile zu finden.

Dazu muss der Nutzer richtig angeleitet werden die relevanten Daten einzugeben. Die Eingabe muss einfach und schnell sein und evtl durch kleine Fragen unterstützt werden damit der Nutzer einen kontinuierlich informativen input Stream liefert.


## Marktteilnehmer mit gleicher/ähnlicher Zielgruppe 
Felix1
Lexware/lexoffice
Sevdesk 
Wiso/Buhl
